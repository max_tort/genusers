## Generate users for mongo

How to start:

`yarn`

Create .env file and specify the `HOST` variable:

`nano .env`

`HOST="mongodb://yourhostname"`

Then you can run the script:

`node index.js`
