require('dotenv').config()
var mongoose = require('mongoose')
var Schema = mongoose.Schema;
var bcrypt = require("bcrypt");
var faker = require('faker');

function setPassword(value) {
  return bcrypt.hashSync(value, 10);
}

const UserSchema = new Schema({
  password: {
    type: String,
    required: false,
    set: setPassword
  },
  email: {
    type: String,
    required: true
    //need pre save to validate number
  },
  firstName: {
    type: String,
    required: true
  },
  lastName: {
    type: String,
    required: true
  },
  country: {
    type: String,
    required: true,
    default: "Kyrgyzstan"
  },
  locality: { //city or village
    type: String,
    required: true,
    default: "Bishkek"
  },
  school: {
    type: String,
    required: false
  },
  grade: {
    type: String,
    required: false
  },
  personalInfo: {
    career: {
      type: String,
      required: false
    },
    purpose: {
      type: String,
      required: false
    }
  },
  avatar: {
    type: String,
    required: true,
    default: "path/to/default/avatar.png"
  },
  isAdmin: {
    type: Boolean,
    required: true,
    default: false
  },
  isActive: {
    type: Boolean,
    required: true,
    default: false
  },
  language: {
    type: String,
    required: true,
    default: "Russian"
  },
  lastLogin: {
    type: String,
    required: false
  },
  xp: {
    type: Number,
    required: true,
    default: 0
  }
});

const numberOfData = 50000

const SCHOOLS = [
  'Илим',
  'УК АФМШЛ #64',
  '#70',
  '#13',
  '#6',
  '#7',
  '#8',
  '#9',
  '#67 Гимназия',
  '#228'
]

async function generateData() {
  const user = mongoose.model('user', UserSchema);
    for (let i = 0; i < numberOfData; i++) {
      let newUser = new user({
        password: setPassword(faker.internet.password()),
        email: faker.internet.email(),
        firstName: faker.name.firstName(),
        lastName: faker.name.lastName(),
        country: faker.address.country(),
        locality: faker.address.state(),
        school: SCHOOLS[faker.random.number(9)],
        grade: (faker.random.number(10) + 1).toString(),
        isActive: true,
        xp: faker.random.number(1000000),
      })
      console.log(`adding user number ${i}`)
      try {
        await newUser.save()
      }
      catch (err) {
        break
        return
      }
    }
}

mongoose.connect(process.env.HOST, {
  useNewUrlParser: true
}, generateData);